from django.db import models

# Createfrom django.db import models  
class Employee(models.Model):  
    Grades= models.IntegerField()  
    Sections= models.CharField(max_length=100)    
    Subjects = models.CharField(max_length=15)  
    class Meta:  
        db_table = "employee"
